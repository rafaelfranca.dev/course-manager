import { Component, OnInit } from '@angular/core'
import { Course } from './course'

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html' 
})
export class CourseListComponent implements OnInit {


  ngOnInit(): void {
    this.courses = [
      {
        id: 1,
        name: "Angular : Forms",
        imageUrl: '/assets/images/forms.png',
        code: 'XPS-8796',
        duration: 120,
        price: 99.99 ,
        rating: 5,
        releaseDate: 'December, 2, 2019'
      },
      {
        id: 2,
        name: "Angular : HTTP",
        imageUrl: '/assets/images/http.png',
        code: 'XPS-8797',
        duration: 100,
        price: 45.99,
        rating: 3.5,
        releaseDate: 'December, 3, 2019'
      },
    ]
   }

  courses: Course[] = [];


}
